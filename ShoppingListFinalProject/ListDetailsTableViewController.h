//
//  ListDetailsTableViewController.h
//  ShoppingListFinalProject
//
//  Created by Orlando Gotera on 12/17/17.
//  Copyright © 2017 Orlando Gotera. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface ListDetailsTableViewController : UITableViewController
{
    AppDelegate *appDelegate;
    NSManagedObjectContext *context;
}
@property (strong) NSManagedObjectModel * aList;
@property (strong, nonatomic) NSMutableArray *itemsFromList;
@end
