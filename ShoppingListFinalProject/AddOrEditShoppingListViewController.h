//
//  AddOrEditShoppingListViewController.h
//  ShoppingListFinalProject
//
//  Created by Orlando Gotera on 12/16/17.
//  Copyright © 2017 Orlando Gotera. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface AddOrEditShoppingListViewController : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate>
{
    AppDelegate *appDelegate;
    NSManagedObjectContext * context;
}
@property (strong) NSManagedObjectModel * aList;
@property (strong, nonatomic) NSMutableArray *itemsFromList;
@end
