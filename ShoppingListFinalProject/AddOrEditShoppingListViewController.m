//
//  AddOrEditShoppingListViewController.m
//  ShoppingListFinalProject
//
//  Created by Orlando Gotera on 12/16/17.
//  Copyright © 2017 Orlando Gotera. All rights reserved.
//

#import "AddOrEditShoppingListViewController.h"

#import "SLists+CoreDataClass.h"
#import "Item+CoreDataClass.h"
#import "AppDelegate.h"


@interface AddOrEditShoppingListViewController ()
@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UITextField *txtItem;
@property (weak, nonatomic) IBOutlet UIPickerView *picker;
@property (weak, nonatomic) IBOutlet UILabel *lblItem;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UIButton *btnAdd;
@property (weak, nonatomic) IBOutlet UIButton *btnDone;
@property (weak, nonatomic) IBOutlet UIButton *btnCategory;
@property (strong, nonatomic) NSMutableArray *currentList;
@property (strong, nonatomic) NSMutableArray *categories;
@property (strong, nonatomic) NSString *selectedCategory;
@end

@implementation AddOrEditShoppingListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSError *error;
    self.categories = (NSMutableArray *)@[@"Dairy", @"Produce", @"Bakery Items", @"Canned Goods", @"Meat", @"Oils and Sauces"];

    appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    context = appDelegate.persistentContainer.viewContext;
    
    if (self.aList) {
        self.txtName.text = [self.aList valueForKey:@"listName"];
        self.txtItem.text = [self.itemsFromList[0] valueForKey:@"itemName"];
    }
    
    // Background image and font colors and buttons
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"projectBG"]];
    self.lblItem.textColor = [UIColor whiteColor];
    self.lblName.textColor = [UIColor whiteColor];
    
    [self.btnAdd.layer setBorderWidth:3.0];
    [self.btnAdd.layer setBorderColor:[[UIColor whiteColor]CGColor]];
    [self.btnAdd setTitleColor:[UIColor colorWithRed:0.39 green:0.86 blue:0.96 alpha:1.0] forState:UIControlStateNormal];
    
    [self.btnDone.layer setBorderWidth:3.0];
    [self.btnDone.layer setBorderColor:[[UIColor whiteColor]CGColor]];
    [self.btnDone setTitleColor:[UIColor colorWithRed:0.39 green:0.86 blue:0.96 alpha:1.0] forState:UIControlStateNormal];
    
    [self.btnCategory.layer setBorderWidth:3.0];
    [self.btnCategory.layer setBorderColor:[[UIColor whiteColor]CGColor]];
    [self.btnCategory setTitleColor:[UIColor colorWithRed:0.39 green:0.86 blue:0.96 alpha:1.0] forState:UIControlStateNormal];
}

-(void)viewDidAppear:(BOOL)animated {
   
    
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
        return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component  {
    return [self.categories count];
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return self.categories[row];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)saveRecord:(UIButton *)sender {
    NSError *error = nil;
    NSFetchRequest *fetchListItems = [[NSFetchRequest alloc] init];
    NSEntityDescription *entityItem = [NSEntityDescription entityForName:@"SLists" inManagedObjectContext:context];
    NSString *listName = self.txtName.text;
    [fetchListItems setEntity:entityItem];
    [fetchListItems setPredicate:[NSPredicate predicateWithFormat:@"listName == %@", listName]];
    self.currentList = (NSMutableArray *)[context executeFetchRequest:fetchListItems error:&error];
    NSLog(@"%@", [self.currentList valueForKey:@"listName"]);
    if ([self.currentList count] == 0) {
        // New List
        SLists *myList = [[SLists alloc]initWithContext:context];
        [myList setValue:self.txtName.text forKey:@"listName"];
        Item *myItem = [[Item alloc]initWithContext:context];
        [myItem setValue:self.txtItem.text forKey:@"itemName"];
        myItem.belongingList = myList;
        NSInteger row = [self.picker selectedRowInComponent:0];
        self.selectedCategory = self.categories[row];
        [myItem setValue:self.selectedCategory forKey:@"itemCategory"];
        
        self.txtItem.text = @"";
        
        //Commit
        if (![context save: &error]) {
            NSLog(@"%@ %@", error, [error localizedDescription] );
        }
    } else {
        Item *myItem = [[Item alloc]initWithContext:context];
        [myItem setValue:self.txtItem.text forKey:@"itemName"];
        myItem.belongingList = self.currentList[0];
        self.txtItem.text = @"";
    }
}

- (IBAction)dismissKeyboard:(UITextField *)sender {
    
}

- (IBAction)btnDone:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


@end
