//
//  ShoppingListTableViewController.m
//  ShoppingListFinalProject
//
//  Created by Orlando Gotera on 12/16/17.
//  Copyright © 2017 Orlando Gotera. All rights reserved.
//

#import "ShoppingListTableViewController.h"
#import "AddOrEditShoppingListViewController.h"
#import "ListDetailsTableViewController.h"

@interface ShoppingListTableViewController ()
@property (strong) NSMutableArray *lists;
@end

@implementation ShoppingListTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
        self.tableView.backgroundView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"projectBG"]];
    appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    context = appDelegate.persistentContainer.viewContext;
}

-(void)viewDidAppear:(BOOL)animated {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"SLists"];
    self.lists = [[context executeFetchRequest:fetchRequest error:nil]mutableCopy];
    
    // refresh the table view
    [self.tableView reloadData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.lists count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"myCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    NSManagedObjectModel *aList = [self.lists objectAtIndex:indexPath.row];
    [cell.textLabel setText:[aList valueForKey:@"listName"]];
    
    // Configure the cell...
    
    if (indexPath.row % 2){
        cell.contentView.backgroundColor = [UIColor colorWithRed:0.11 green:0.82 blue:0.35 alpha:1.0];
        cell.textLabel.backgroundColor = [UIColor colorWithRed:0.11 green:0.82 blue:0.35 alpha:1.0];
    }
    else{
        cell.contentView.backgroundColor = [UIColor colorWithRed:0.27 green:0.89 blue:0.48 alpha:1.0];
        cell.textLabel.backgroundColor = [UIColor colorWithRed:0.27 green:0.89 blue:0.48 alpha:1.0];
    }
    
    
    return cell;
}



// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [context deleteObject:[self.lists objectAtIndex:indexPath.row]];
        
        NSError *error = nil;
        if(![context save:&error]){
            NSLog(@"%@ %@", error, [error localizedDescription]);
        }
        [self.lists removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
    }
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"selectedCell"]) {
//        NSError *error;
        //Pass the selected object to the new view controller
        NSManagedObjectModel *selectedList = [self.lists objectAtIndex:[[self.tableView indexPathForSelectedRow]row]];
        ListDetailsTableViewController *updateList = segue.destinationViewController;
        updateList.aList = selectedList;
        
        //fetching all the items w/in the list
//        NSFetchRequest *fetchListItems = [[NSFetchRequest alloc] init];
//        NSEntityDescription *entityItem = [NSEntityDescription entityForName:@"Item" inManagedObjectContext:context];
//        [fetchListItems setEntity:entityItem];
//        [fetchListItems setPredicate:[NSPredicate predicateWithFormat:@"belongingList == %@", selectedList]];
//        updateList.itemsFromList = (NSMutableArray*)[context executeFetchRequest:fetchListItems error:&error];
        
    }
}


@end
